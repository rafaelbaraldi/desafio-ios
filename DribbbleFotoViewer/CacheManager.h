//
//  CacheManager.h
//
//  Created by Cesar Bonadio on 29/11/11.
//  Copyright (c) 2011 Viewit. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CacheManager : NSObject {
}

+ (CacheManager *)sharedCacheManager;

- (void)gravarCache:(NSData *)data doEndereco:(NSString *)endereco;
- (NSData *)cacheDoEndereco:(NSString *)endereco;
- (void)limparCacheAntigo;

@end