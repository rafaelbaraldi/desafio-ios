//
//  UIImageViewRemota.h
//  RemoteImage
//
//  Created by Cesar Bonadio on 29/11/11.
//  Copyright (c) 2011 Viewit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageViewRemota : UIImageView {
    UIActivityIndicatorView *spinner; //spinner sobre a imageView enquanto a imagem é carregada
    NSString *enderecoImagem; //endereço http da imagem a ser carregada
    NSOperationQueue *queue; //fila de operações para tratar o carregamento em paralelo
}
@property (nonatomic, copy) NSString *enderecoImagem;
@property (nonatomic, copy) NSString *spinnerColor; // B branco  P preto

- (void) criaSpinner;

@end
