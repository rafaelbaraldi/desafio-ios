//
//  ViewController.h
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 08/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseClass.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property int pagina;

@property BaseClass* responseObj;

@property NSString* baseURLString;

@property (weak, nonatomic) IBOutlet UITableView *table;

@property BOOL estaAtualizando;

@property UIRefreshControl *refreshControl;

@end

