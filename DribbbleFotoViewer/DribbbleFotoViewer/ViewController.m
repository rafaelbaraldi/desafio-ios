//
//  ViewController.m
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 08/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "Shots.h"
#import "CustomTableViewCell.h"
#import "FotoDetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _refreshControl = [[UIRefreshControl alloc] init];
    _refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Carregando..."];
    [_refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [_table addSubview:_refreshControl];
    
    _estaAtualizando = NO;
    
    _pagina = 1;
    _baseURLString = @"http://api.dribbble.com/shots/popular?page=";
    
    [self getContentOfPage:_pagina];
}

#pragma mark SEGUE

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier rangeOfString:@"showDetail"].length > 0) {
        
        NSIndexPath *path = [_table indexPathForSelectedRow];
        Shots *foto  = [_responseObj.shots objectAtIndex:path.row];
        
        FotoDetailViewController* destinationViewController  = segue.destinationViewController;
        
        destinationViewController.foto = foto;
    }
}

#pragma mark REQUEST

-(void)getContentOfPage:(int) page{
    NSString *urlString = [NSString stringWithFormat:@"%@%i", _baseURLString, page];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        NSString* responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* respondeSDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        
        BaseClass* object = [BaseClass modelObjectWithDictionary:respondeSDict];
        
        if (_responseObj == nil || page == 1) {
            _responseObj = object;
        }
        else{
            NSMutableArray* newArray = [NSMutableArray arrayWithArray:_responseObj.shots];
            [newArray addObjectsFromArray:object.shots];
            _responseObj.shots = [NSArray arrayWithArray:newArray];
        }
        
        [_table reloadData];
        
        _estaAtualizando = NO;
        
        [_refreshControl endRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error.description);
    }];
    
    [operation start];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    // Do your job, when done:
    
    _pagina = 1;
    
    [self getContentOfPage:_pagina];
}

#pragma mark TABLEVIEW DATASOURCE

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (cell.tag == 10) {
        
        if (_pagina < _responseObj.pages) {
            
            if(!_estaAtualizando){
                _estaAtualizando = YES;
    
                _pagina++;
                
                [self getContentOfPage:_pagina];
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < _responseObj.shots.count) {
        return [self fotoCellForTableView:tableView rowAtIndexPath:indexPath];
    } else if(_pagina < _responseObj.pages){
        return [self loadingCellForTableView:tableView];
    }else{
        return [[UITableViewCell alloc] init];
    }
}

-(CustomTableViewCell*)fotoCellForTableView:(UITableView *)tableView rowAtIndexPath:(NSIndexPath *)indexPath{
    CustomTableViewCell *cell = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FotoCell"];
    if (cell == nil) {
        cell = [[CustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FotoCell"];
    }
    
    Shots* foto = [_responseObj.shots objectAtIndex:indexPath.row];
    
    cell.image.enderecoImagem = foto.imageTeaserUrl;
    cell.lblTitle.text = foto.title;
    
    return cell;
}

- (UITableViewCell *)loadingCellForTableView:(UITableView *)tableView {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    //    if(cell == nil){
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LoadCell"];
    //    }
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = 10;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _responseObj.shots.count + 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
