//
//  BaseClass.m
//
//  Created by Marcelo  on 09/08/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "BaseClass.h"
#import "Shots.h"


NSString *const kBaseClassShots = @"shots";
NSString *const kBaseClassPages = @"pages";
NSString *const kBaseClassPerPage = @"per_page";
NSString *const kBaseClassTotal = @"total";
NSString *const kBaseClassPage = @"page";


@interface BaseClass ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BaseClass

@synthesize shots = _shots;
@synthesize pages = _pages;
@synthesize perPage = _perPage;
@synthesize total = _total;
@synthesize page = _page;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedShots = [dict objectForKey:kBaseClassShots];
    NSMutableArray *parsedShots = [NSMutableArray array];
    if ([receivedShots isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedShots) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedShots addObject:[Shots modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedShots isKindOfClass:[NSDictionary class]]) {
       [parsedShots addObject:[Shots modelObjectWithDictionary:(NSDictionary *)receivedShots]];
    }

    self.shots = [NSArray arrayWithArray:parsedShots];
            self.pages = [[self objectOrNilForKey:kBaseClassPages fromDictionary:dict] doubleValue];
            self.perPage = [[self objectOrNilForKey:kBaseClassPerPage fromDictionary:dict] doubleValue];
            self.total = [[self objectOrNilForKey:kBaseClassTotal fromDictionary:dict] doubleValue];
            self.page = [self objectOrNilForKey:kBaseClassPage fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForShots = [NSMutableArray array];
    for (NSObject *subArrayObject in self.shots) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForShots addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForShots addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForShots] forKey:kBaseClassShots];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pages] forKey:kBaseClassPages];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perPage] forKey:kBaseClassPerPage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.total] forKey:kBaseClassTotal];
    [mutableDict setValue:self.page forKey:kBaseClassPage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.shots = [aDecoder decodeObjectForKey:kBaseClassShots];
    self.pages = [aDecoder decodeDoubleForKey:kBaseClassPages];
    self.perPage = [aDecoder decodeDoubleForKey:kBaseClassPerPage];
    self.total = [aDecoder decodeDoubleForKey:kBaseClassTotal];
    self.page = [aDecoder decodeObjectForKey:kBaseClassPage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_shots forKey:kBaseClassShots];
    [aCoder encodeDouble:_pages forKey:kBaseClassPages];
    [aCoder encodeDouble:_perPage forKey:kBaseClassPerPage];
    [aCoder encodeDouble:_total forKey:kBaseClassTotal];
    [aCoder encodeObject:_page forKey:kBaseClassPage];
}

- (id)copyWithZone:(NSZone *)zone
{
    BaseClass *copy = [[BaseClass alloc] init];
    
    if (copy) {

        copy.shots = [self.shots copyWithZone:zone];
        copy.pages = self.pages;
        copy.perPage = self.perPage;
        copy.total = self.total;
        copy.page = [self.page copyWithZone:zone];
    }
    
    return copy;
}


@end
