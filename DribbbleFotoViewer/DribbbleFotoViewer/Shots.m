//
//  Shots.m
//
//  Created by Marcelo  on 09/08/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Shots.h"
#import "Player.h"


NSString *const kShotsId = @"id";
NSString *const kShotsDescription = @"description";
NSString *const kShotsImageTeaserUrl = @"image_teaser_url";
NSString *const kShotsReboundSourceId = @"rebound_source_id";
NSString *const kShotsViewsCount = @"views_count";
NSString *const kShotsWidth = @"width";
NSString *const kShotsUrl = @"url";
NSString *const kShotsImageUrl = @"image_url";
NSString *const kShotsCreatedAt = @"created_at";
NSString *const kShotsTitle = @"title";
NSString *const kShotsImage400Url = @"image_400_url";
NSString *const kShotsReboundsCount = @"rebounds_count";
NSString *const kShotsHeight = @"height";
NSString *const kShotsPlayer = @"player";
NSString *const kShotsLikesCount = @"likes_count";
NSString *const kShotsCommentsCount = @"comments_count";
NSString *const kShotsShortUrl = @"short_url";


@interface Shots ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Shots

@synthesize shotsIdentifier = _shotsIdentifier;
@synthesize shotsDescription = _shotsDescription;
@synthesize imageTeaserUrl = _imageTeaserUrl;
@synthesize reboundSourceId = _reboundSourceId;
@synthesize viewsCount = _viewsCount;
@synthesize width = _width;
@synthesize url = _url;
@synthesize imageUrl = _imageUrl;
@synthesize createdAt = _createdAt;
@synthesize title = _title;
@synthesize image400Url = _image400Url;
@synthesize reboundsCount = _reboundsCount;
@synthesize height = _height;
@synthesize player = _player;
@synthesize likesCount = _likesCount;
@synthesize commentsCount = _commentsCount;
@synthesize shortUrl = _shortUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.shotsIdentifier = [[self objectOrNilForKey:kShotsId fromDictionary:dict] doubleValue];
            self.shotsDescription = [self objectOrNilForKey:kShotsDescription fromDictionary:dict];
            self.imageTeaserUrl = [self objectOrNilForKey:kShotsImageTeaserUrl fromDictionary:dict];
            self.reboundSourceId = [self objectOrNilForKey:kShotsReboundSourceId fromDictionary:dict];
            self.viewsCount = [[self objectOrNilForKey:kShotsViewsCount fromDictionary:dict] doubleValue];
            self.width = [[self objectOrNilForKey:kShotsWidth fromDictionary:dict] doubleValue];
            self.url = [self objectOrNilForKey:kShotsUrl fromDictionary:dict];
            self.imageUrl = [self objectOrNilForKey:kShotsImageUrl fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kShotsCreatedAt fromDictionary:dict];
            self.title = [self objectOrNilForKey:kShotsTitle fromDictionary:dict];
            self.image400Url = [self objectOrNilForKey:kShotsImage400Url fromDictionary:dict];
            self.reboundsCount = [[self objectOrNilForKey:kShotsReboundsCount fromDictionary:dict] doubleValue];
            self.height = [[self objectOrNilForKey:kShotsHeight fromDictionary:dict] doubleValue];
            self.player = [Player modelObjectWithDictionary:[dict objectForKey:kShotsPlayer]];
            self.likesCount = [[self objectOrNilForKey:kShotsLikesCount fromDictionary:dict] doubleValue];
            self.commentsCount = [[self objectOrNilForKey:kShotsCommentsCount fromDictionary:dict] doubleValue];
            self.shortUrl = [self objectOrNilForKey:kShotsShortUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shotsIdentifier] forKey:kShotsId];
    [mutableDict setValue:self.shotsDescription forKey:kShotsDescription];
    [mutableDict setValue:self.imageTeaserUrl forKey:kShotsImageTeaserUrl];
    [mutableDict setValue:self.reboundSourceId forKey:kShotsReboundSourceId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.viewsCount] forKey:kShotsViewsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.width] forKey:kShotsWidth];
    [mutableDict setValue:self.url forKey:kShotsUrl];
    [mutableDict setValue:self.imageUrl forKey:kShotsImageUrl];
    [mutableDict setValue:self.createdAt forKey:kShotsCreatedAt];
    [mutableDict setValue:self.title forKey:kShotsTitle];
    [mutableDict setValue:self.image400Url forKey:kShotsImage400Url];
    [mutableDict setValue:[NSNumber numberWithDouble:self.reboundsCount] forKey:kShotsReboundsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.height] forKey:kShotsHeight];
    [mutableDict setValue:[self.player dictionaryRepresentation] forKey:kShotsPlayer];
    [mutableDict setValue:[NSNumber numberWithDouble:self.likesCount] forKey:kShotsLikesCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentsCount] forKey:kShotsCommentsCount];
    [mutableDict setValue:self.shortUrl forKey:kShotsShortUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.shotsIdentifier = [aDecoder decodeDoubleForKey:kShotsId];
    self.shotsDescription = [aDecoder decodeObjectForKey:kShotsDescription];
    self.imageTeaserUrl = [aDecoder decodeObjectForKey:kShotsImageTeaserUrl];
    self.reboundSourceId = [aDecoder decodeObjectForKey:kShotsReboundSourceId];
    self.viewsCount = [aDecoder decodeDoubleForKey:kShotsViewsCount];
    self.width = [aDecoder decodeDoubleForKey:kShotsWidth];
    self.url = [aDecoder decodeObjectForKey:kShotsUrl];
    self.imageUrl = [aDecoder decodeObjectForKey:kShotsImageUrl];
    self.createdAt = [aDecoder decodeObjectForKey:kShotsCreatedAt];
    self.title = [aDecoder decodeObjectForKey:kShotsTitle];
    self.image400Url = [aDecoder decodeObjectForKey:kShotsImage400Url];
    self.reboundsCount = [aDecoder decodeDoubleForKey:kShotsReboundsCount];
    self.height = [aDecoder decodeDoubleForKey:kShotsHeight];
    self.player = [aDecoder decodeObjectForKey:kShotsPlayer];
    self.likesCount = [aDecoder decodeDoubleForKey:kShotsLikesCount];
    self.commentsCount = [aDecoder decodeDoubleForKey:kShotsCommentsCount];
    self.shortUrl = [aDecoder decodeObjectForKey:kShotsShortUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_shotsIdentifier forKey:kShotsId];
    [aCoder encodeObject:_shotsDescription forKey:kShotsDescription];
    [aCoder encodeObject:_imageTeaserUrl forKey:kShotsImageTeaserUrl];
    [aCoder encodeObject:_reboundSourceId forKey:kShotsReboundSourceId];
    [aCoder encodeDouble:_viewsCount forKey:kShotsViewsCount];
    [aCoder encodeDouble:_width forKey:kShotsWidth];
    [aCoder encodeObject:_url forKey:kShotsUrl];
    [aCoder encodeObject:_imageUrl forKey:kShotsImageUrl];
    [aCoder encodeObject:_createdAt forKey:kShotsCreatedAt];
    [aCoder encodeObject:_title forKey:kShotsTitle];
    [aCoder encodeObject:_image400Url forKey:kShotsImage400Url];
    [aCoder encodeDouble:_reboundsCount forKey:kShotsReboundsCount];
    [aCoder encodeDouble:_height forKey:kShotsHeight];
    [aCoder encodeObject:_player forKey:kShotsPlayer];
    [aCoder encodeDouble:_likesCount forKey:kShotsLikesCount];
    [aCoder encodeDouble:_commentsCount forKey:kShotsCommentsCount];
    [aCoder encodeObject:_shortUrl forKey:kShotsShortUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    Shots *copy = [[Shots alloc] init];
    
    if (copy) {

        copy.shotsIdentifier = self.shotsIdentifier;
        copy.shotsDescription = [self.shotsDescription copyWithZone:zone];
        copy.imageTeaserUrl = [self.imageTeaserUrl copyWithZone:zone];
        copy.reboundSourceId = [self.reboundSourceId copyWithZone:zone];
        copy.viewsCount = self.viewsCount;
        copy.width = self.width;
        copy.url = [self.url copyWithZone:zone];
        copy.imageUrl = [self.imageUrl copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.image400Url = [self.image400Url copyWithZone:zone];
        copy.reboundsCount = self.reboundsCount;
        copy.height = self.height;
        copy.player = [self.player copyWithZone:zone];
        copy.likesCount = self.likesCount;
        copy.commentsCount = self.commentsCount;
        copy.shortUrl = [self.shortUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
