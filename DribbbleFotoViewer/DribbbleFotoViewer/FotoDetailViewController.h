//
//  FotoDetailViewController.h
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 09/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shots.h"
#import "UIImageViewRemota.h"

@interface FotoDetailViewController : UIViewController

@property Shots* foto;

@property (weak, nonatomic) IBOutlet UIImageViewRemota *imagem;

@property (weak, nonatomic) IBOutlet UILabel *titulo;

@property (weak, nonatomic) IBOutlet UITextView *descricao;

@property (weak, nonatomic) IBOutlet UIImageViewRemota *avatar;

@property (weak, nonatomic) IBOutlet UILabel *autor;

@end
