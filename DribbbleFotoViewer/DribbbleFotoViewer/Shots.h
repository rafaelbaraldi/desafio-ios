//
//  Shots.h
//
//  Created by Marcelo  on 09/08/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Player;

@interface Shots : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double shotsIdentifier;
@property (nonatomic, strong) NSString *shotsDescription;
@property (nonatomic, strong) NSString *imageTeaserUrl;
@property (nonatomic, assign) id reboundSourceId;
@property (nonatomic, assign) double viewsCount;
@property (nonatomic, assign) double width;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image400Url;
@property (nonatomic, assign) double reboundsCount;
@property (nonatomic, assign) double height;
@property (nonatomic, strong) Player *player;
@property (nonatomic, assign) double likesCount;
@property (nonatomic, assign) double commentsCount;
@property (nonatomic, strong) NSString *shortUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
