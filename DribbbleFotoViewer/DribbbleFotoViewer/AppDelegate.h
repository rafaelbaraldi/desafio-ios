//
//  AppDelegate.h
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 08/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

