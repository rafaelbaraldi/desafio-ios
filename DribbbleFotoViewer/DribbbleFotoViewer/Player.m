//
//  Player.m
//
//  Created by Marcelo  on 09/08/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import "Player.h"


NSString *const kPlayerTwitterScreenName = @"twitter_screen_name";
NSString *const kPlayerLocation = @"location";
NSString *const kPlayerDrafteesCount = @"draftees_count";
NSString *const kPlayerFollowingCount = @"following_count";
NSString *const kPlayerUrl = @"url";
NSString *const kPlayerWebsiteUrl = @"website_url";
NSString *const kPlayerCommentsCount = @"comments_count";
NSString *const kPlayerShotsCount = @"shots_count";
NSString *const kPlayerReboundsReceivedCount = @"rebounds_received_count";
NSString *const kPlayerDraftedByPlayerId = @"drafted_by_player_id";
NSString *const kPlayerAvatarUrl = @"avatar_url";
NSString *const kPlayerName = @"name";
NSString *const kPlayerId = @"id";
NSString *const kPlayerReboundsCount = @"rebounds_count";
NSString *const kPlayerFollowersCount = @"followers_count";
NSString *const kPlayerLikesReceivedCount = @"likes_received_count";
NSString *const kPlayerLikesCount = @"likes_count";
NSString *const kPlayerCreatedAt = @"created_at";
NSString *const kPlayerCommentsReceivedCount = @"comments_received_count";
NSString *const kPlayerUsername = @"username";


@interface Player ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Player

@synthesize twitterScreenName = _twitterScreenName;
@synthesize location = _location;
@synthesize drafteesCount = _drafteesCount;
@synthesize followingCount = _followingCount;
@synthesize url = _url;
@synthesize websiteUrl = _websiteUrl;
@synthesize commentsCount = _commentsCount;
@synthesize shotsCount = _shotsCount;
@synthesize reboundsReceivedCount = _reboundsReceivedCount;
@synthesize draftedByPlayerId = _draftedByPlayerId;
@synthesize avatarUrl = _avatarUrl;
@synthesize name = _name;
@synthesize playerIdentifier = _playerIdentifier;
@synthesize reboundsCount = _reboundsCount;
@synthesize followersCount = _followersCount;
@synthesize likesReceivedCount = _likesReceivedCount;
@synthesize likesCount = _likesCount;
@synthesize createdAt = _createdAt;
@synthesize commentsReceivedCount = _commentsReceivedCount;
@synthesize username = _username;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.twitterScreenName = [self objectOrNilForKey:kPlayerTwitterScreenName fromDictionary:dict];
            self.location = [self objectOrNilForKey:kPlayerLocation fromDictionary:dict];
            self.drafteesCount = [[self objectOrNilForKey:kPlayerDrafteesCount fromDictionary:dict] doubleValue];
            self.followingCount = [[self objectOrNilForKey:kPlayerFollowingCount fromDictionary:dict] doubleValue];
            self.url = [self objectOrNilForKey:kPlayerUrl fromDictionary:dict];
            self.websiteUrl = [self objectOrNilForKey:kPlayerWebsiteUrl fromDictionary:dict];
            self.commentsCount = [[self objectOrNilForKey:kPlayerCommentsCount fromDictionary:dict] doubleValue];
            self.shotsCount = [[self objectOrNilForKey:kPlayerShotsCount fromDictionary:dict] doubleValue];
            self.reboundsReceivedCount = [[self objectOrNilForKey:kPlayerReboundsReceivedCount fromDictionary:dict] doubleValue];
            self.draftedByPlayerId = [[self objectOrNilForKey:kPlayerDraftedByPlayerId fromDictionary:dict] doubleValue];
            self.avatarUrl = [self objectOrNilForKey:kPlayerAvatarUrl fromDictionary:dict];
            self.name = [self objectOrNilForKey:kPlayerName fromDictionary:dict];
            self.playerIdentifier = [[self objectOrNilForKey:kPlayerId fromDictionary:dict] doubleValue];
            self.reboundsCount = [[self objectOrNilForKey:kPlayerReboundsCount fromDictionary:dict] doubleValue];
            self.followersCount = [[self objectOrNilForKey:kPlayerFollowersCount fromDictionary:dict] doubleValue];
            self.likesReceivedCount = [[self objectOrNilForKey:kPlayerLikesReceivedCount fromDictionary:dict] doubleValue];
            self.likesCount = [[self objectOrNilForKey:kPlayerLikesCount fromDictionary:dict] doubleValue];
            self.createdAt = [self objectOrNilForKey:kPlayerCreatedAt fromDictionary:dict];
            self.commentsReceivedCount = [[self objectOrNilForKey:kPlayerCommentsReceivedCount fromDictionary:dict] doubleValue];
            self.username = [self objectOrNilForKey:kPlayerUsername fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.twitterScreenName forKey:kPlayerTwitterScreenName];
    [mutableDict setValue:self.location forKey:kPlayerLocation];
    [mutableDict setValue:[NSNumber numberWithDouble:self.drafteesCount] forKey:kPlayerDrafteesCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.followingCount] forKey:kPlayerFollowingCount];
    [mutableDict setValue:self.url forKey:kPlayerUrl];
    [mutableDict setValue:self.websiteUrl forKey:kPlayerWebsiteUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentsCount] forKey:kPlayerCommentsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.shotsCount] forKey:kPlayerShotsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.reboundsReceivedCount] forKey:kPlayerReboundsReceivedCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.draftedByPlayerId] forKey:kPlayerDraftedByPlayerId];
    [mutableDict setValue:self.avatarUrl forKey:kPlayerAvatarUrl];
    [mutableDict setValue:self.name forKey:kPlayerName];
    [mutableDict setValue:[NSNumber numberWithDouble:self.playerIdentifier] forKey:kPlayerId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.reboundsCount] forKey:kPlayerReboundsCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.followersCount] forKey:kPlayerFollowersCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.likesReceivedCount] forKey:kPlayerLikesReceivedCount];
    [mutableDict setValue:[NSNumber numberWithDouble:self.likesCount] forKey:kPlayerLikesCount];
    [mutableDict setValue:self.createdAt forKey:kPlayerCreatedAt];
    [mutableDict setValue:[NSNumber numberWithDouble:self.commentsReceivedCount] forKey:kPlayerCommentsReceivedCount];
    [mutableDict setValue:self.username forKey:kPlayerUsername];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.twitterScreenName = [aDecoder decodeObjectForKey:kPlayerTwitterScreenName];
    self.location = [aDecoder decodeObjectForKey:kPlayerLocation];
    self.drafteesCount = [aDecoder decodeDoubleForKey:kPlayerDrafteesCount];
    self.followingCount = [aDecoder decodeDoubleForKey:kPlayerFollowingCount];
    self.url = [aDecoder decodeObjectForKey:kPlayerUrl];
    self.websiteUrl = [aDecoder decodeObjectForKey:kPlayerWebsiteUrl];
    self.commentsCount = [aDecoder decodeDoubleForKey:kPlayerCommentsCount];
    self.shotsCount = [aDecoder decodeDoubleForKey:kPlayerShotsCount];
    self.reboundsReceivedCount = [aDecoder decodeDoubleForKey:kPlayerReboundsReceivedCount];
    self.draftedByPlayerId = [aDecoder decodeDoubleForKey:kPlayerDraftedByPlayerId];
    self.avatarUrl = [aDecoder decodeObjectForKey:kPlayerAvatarUrl];
    self.name = [aDecoder decodeObjectForKey:kPlayerName];
    self.playerIdentifier = [aDecoder decodeDoubleForKey:kPlayerId];
    self.reboundsCount = [aDecoder decodeDoubleForKey:kPlayerReboundsCount];
    self.followersCount = [aDecoder decodeDoubleForKey:kPlayerFollowersCount];
    self.likesReceivedCount = [aDecoder decodeDoubleForKey:kPlayerLikesReceivedCount];
    self.likesCount = [aDecoder decodeDoubleForKey:kPlayerLikesCount];
    self.createdAt = [aDecoder decodeObjectForKey:kPlayerCreatedAt];
    self.commentsReceivedCount = [aDecoder decodeDoubleForKey:kPlayerCommentsReceivedCount];
    self.username = [aDecoder decodeObjectForKey:kPlayerUsername];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_twitterScreenName forKey:kPlayerTwitterScreenName];
    [aCoder encodeObject:_location forKey:kPlayerLocation];
    [aCoder encodeDouble:_drafteesCount forKey:kPlayerDrafteesCount];
    [aCoder encodeDouble:_followingCount forKey:kPlayerFollowingCount];
    [aCoder encodeObject:_url forKey:kPlayerUrl];
    [aCoder encodeObject:_websiteUrl forKey:kPlayerWebsiteUrl];
    [aCoder encodeDouble:_commentsCount forKey:kPlayerCommentsCount];
    [aCoder encodeDouble:_shotsCount forKey:kPlayerShotsCount];
    [aCoder encodeDouble:_reboundsReceivedCount forKey:kPlayerReboundsReceivedCount];
    [aCoder encodeDouble:_draftedByPlayerId forKey:kPlayerDraftedByPlayerId];
    [aCoder encodeObject:_avatarUrl forKey:kPlayerAvatarUrl];
    [aCoder encodeObject:_name forKey:kPlayerName];
    [aCoder encodeDouble:_playerIdentifier forKey:kPlayerId];
    [aCoder encodeDouble:_reboundsCount forKey:kPlayerReboundsCount];
    [aCoder encodeDouble:_followersCount forKey:kPlayerFollowersCount];
    [aCoder encodeDouble:_likesReceivedCount forKey:kPlayerLikesReceivedCount];
    [aCoder encodeDouble:_likesCount forKey:kPlayerLikesCount];
    [aCoder encodeObject:_createdAt forKey:kPlayerCreatedAt];
    [aCoder encodeDouble:_commentsReceivedCount forKey:kPlayerCommentsReceivedCount];
    [aCoder encodeObject:_username forKey:kPlayerUsername];
}

- (id)copyWithZone:(NSZone *)zone
{
    Player *copy = [[Player alloc] init];
    
    if (copy) {

        copy.twitterScreenName = [self.twitterScreenName copyWithZone:zone];
        copy.location = [self.location copyWithZone:zone];
        copy.drafteesCount = self.drafteesCount;
        copy.followingCount = self.followingCount;
        copy.url = [self.url copyWithZone:zone];
        copy.websiteUrl = [self.websiteUrl copyWithZone:zone];
        copy.commentsCount = self.commentsCount;
        copy.shotsCount = self.shotsCount;
        copy.reboundsReceivedCount = self.reboundsReceivedCount;
        copy.draftedByPlayerId = self.draftedByPlayerId;
        copy.avatarUrl = [self.avatarUrl copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.playerIdentifier = self.playerIdentifier;
        copy.reboundsCount = self.reboundsCount;
        copy.followersCount = self.followersCount;
        copy.likesReceivedCount = self.likesReceivedCount;
        copy.likesCount = self.likesCount;
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.commentsReceivedCount = self.commentsReceivedCount;
        copy.username = [self.username copyWithZone:zone];
    }
    
    return copy;
}


@end
