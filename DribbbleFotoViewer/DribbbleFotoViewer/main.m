//
//  main.m
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 08/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
