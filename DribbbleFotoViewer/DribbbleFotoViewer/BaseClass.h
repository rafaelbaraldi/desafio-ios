//
//  BaseClass.h
//
//  Created by Marcelo  on 09/08/15
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BaseClass : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *shots;
@property (nonatomic, assign) double pages;
@property (nonatomic, assign) double perPage;
@property (nonatomic, assign) double total;
@property (nonatomic, strong) NSString *page;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
