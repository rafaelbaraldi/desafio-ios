//
//  CustomTableViewCell.m
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 09/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
