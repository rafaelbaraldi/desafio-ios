//
//  CustomTableViewCell.h
//  DribbbleFotoViewer
//
//  Created by Rafael Baraldi on 09/08/15.
//  Copyright (c) 2015 Rafael BAraldi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewRemota.h"

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageViewRemota *image;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
