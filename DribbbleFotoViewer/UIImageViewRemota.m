//
//  UIImageViewRemota.m
//  RemoteImage
//
//  Created by Cesar Bonadio on 29/11/11.
//  Copyright (c) 2011 Viewit. All rights reserved.
//

#import "UIImageViewRemota.h"
#import "CacheManager.h"


@implementation UIImageViewRemota
@synthesize enderecoImagem;
@synthesize spinnerColor;

- (id)initWithFrame:(CGRect)rect {
    if (self = [super initWithFrame:rect]) {
        [self criaSpinner];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self criaSpinner];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    spinner.center = [self convertPoint:self.center fromView:self.superview];
}

- (void)criaSpinner {
    //NSLog(@"cria spinner");
    
    if ( [spinnerColor isEqualToString:@"P"])
    {
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    } else if ( [spinnerColor isEqualToString:@"W"]) {
        // branco
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    } else {
        // default cinza
        spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    [self addSubview:spinner];
}

- (void)setEnderecoImagem:(NSString *)endereco {
    //NSLog(@"setEnderecoImagem");
    if ([endereco length] == 0) { //caso o endereco seja nulo ou uma string vazia, remove a imagem atual da imageView
        enderecoImagem = nil;
        self.image = nil;
    }
    else if (endereco != enderecoImagem) { //se o endereco for diferente do atual, atualiza a imagem
        enderecoImagem = [endereco copy];
        
        self.image = nil; //remove a imagem atual para evitar superposições 
        
        if (!queue) {
            queue = [[NSOperationQueue alloc] init];
        }
        
        [queue cancelAllOperations]; //cancela qualquer tarefa de carregamento pendente (evita que chamadas consecutivas atrasem o carregamento inutilmente)
        
        [spinner startAnimating]; //dispara o spinner
        
        NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(carregaImagemEmBackground) object:nil];
        [queue addOperation:operation]; //envia a tarefa para ser executada em paralelo
    }
}

- (void)carregaImagemEmBackground {
    NSData *data = [[CacheManager sharedCacheManager] cacheDoEndereco:enderecoImagem];
    if (!data) {
        // somente se não houver cache, fazemos o carregamento remoto
        data = [NSData dataWithContentsOfURL:[NSURL URLWithString:enderecoImagem]];
        [[CacheManager sharedCacheManager] gravarCache:data doEndereco:enderecoImagem];
    }
    
    
    UIImage *imagem = [[UIImage alloc] initWithData:data];
    [self performSelectorOnMainThread:@selector(exibeImagemCarregada:) withObject:imagem waitUntilDone:YES]; //solicita a main thread que exiba a imagem e interrompa o spinner
}

- (void)exibeImagemCarregada:(UIImage *)imagem {
    self.image = imagem;
    [spinner stopAnimating];
}



@end
