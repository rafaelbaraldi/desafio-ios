//
//  CacheManager.m
//
//  Created by Cesar Bonadio on 29/11/11.
//  Copyright (c) 2011 Viewit. All rights reserved.
//

#import "CacheManager.h"
#import "SynthesizeSingleton.h"

#ifndef MAXIMA_IDADE_CACHE

#define MAXIMA_IDADE_CACHE 5 * 60

#endif

@interface CacheManager (Private)

- (NSString *)diretorioDeCache;

@end

@implementation CacheManager

SYNTHESIZE_SINGLETON_FOR_CLASS(CacheManager)

#pragma mark -
#pragma mark Públicos

- (void)gravarCache:(NSData *)data doEndereco:(NSString *)endereco {
    NSURL *url = [NSURL URLWithString:endereco];

    NSString *nomeDoArquivo = [endereco lastPathComponent];
    NSString *caminhoDoArquivo = [[url path] stringByDeletingLastPathComponent];

    NSString *diretorio = [[self diretorioDeCache] stringByAppendingPathComponent:caminhoDoArquivo];
    NSString *caminhoCompleto = [diretorio stringByAppendingPathComponent:nomeDoArquivo];

    if (![[NSFileManager defaultManager] fileExistsAtPath:caminhoCompleto]) {
        NSError *erro = nil;

        if (![[NSFileManager defaultManager] fileExistsAtPath:diretorio]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:diretorio withIntermediateDirectories:YES attributes:nil error:&erro];
            if (erro) {
                NSLog(@"erro ao criar diretorio para %@: %@", endereco, erro);
                return;
            }
        }

        if (![data writeToFile:caminhoCompleto atomically:YES]) {
            NSLog(@"erro ao gravar cache para %@", endereco);
        }
    }
}

- (NSData *)cacheDoEndereco:(NSString *)endereco {
    NSURL *url = [NSURL URLWithString:endereco];

    NSString *caminhoCompleto = [[self diretorioDeCache] stringByAppendingPathComponent:[url path]];

    if ([[NSFileManager defaultManager] fileExistsAtPath:caminhoCompleto])
        return [NSData dataWithContentsOfFile:caminhoCompleto];

    return nil;
}

- (void)limparCacheAntigo {
//	NSLog(@"maxima idade de cache: %d", MAXIMA_IDADE_CACHE);
//	
//    NSMutableArray *arquivosVelhos = [[NSMutableArray alloc] init];
//
//    NSDirectoryEnumerator *en = [[NSFileManager defaultManager] enumeratorAtPath:[self diretorioDeCache]];
//    for (NSString *caminho in en) {
//        NSDictionary *attrs = [en fileAttributes];
//        if ([attrs objectForKey:NSFileType] == NSFileTypeRegular) {
//            NSDate *date = [attrs objectForKey:NSFileModificationDate];
//
//            if (fabs([date timeIntervalSinceNow]) > MAXIMA_IDADE_CACHE) {
//                [arquivosVelhos addObject:caminho];
//            }
//        }
//    }
//
//    for (NSString *caminho in arquivosVelhos) {
//        NSLog(@"Apagando cache em %@", caminho);
//        NSError * erro = nil;
//        [[NSFileManager defaultManager] removeItemAtPath:[[self diretorioDeCache] stringByAppendingPathComponent:caminho] error:&erro];
//        if (erro) {
//            NSLog(@"Erro ao apagar %@: %@", caminho, erro);
//        }
//    }

}

#pragma mark -
#pragma mark Privados

- (NSString *)diretorioDeCache {
    return [NSTemporaryDirectory () stringByAppendingPathComponent:@"cache"];
}

@end